import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

	user: User;

	private currentUserSubject: BehaviorSubject<User>;

    public currentUser: Observable<User>;

	constructor() {
		this.currentUserSubject = 
        	new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));

        this.currentUser = this.currentUserSubject.asObservable();
	}

	public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

	login(username: string, password: string) {
        this.user = 
            {
                id: 1,
                firstName: "Samuel",
                lastName: "Teshome",
                userName: username,
                password: "",
                active: true,
                token: ""
            };
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        this.currentUserSubject.next(this.user);
    }

    logout() {
    	localStorage.removeItem('currentUser');
    	this.currentUserSubject.next(null);
    }

}
