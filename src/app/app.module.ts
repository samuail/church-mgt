import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { ChartsModule } from 'ng2-charts';

import { Ng2TelInputModule } from 'ng2-tel-input';

import { MatPaginatorIntl } from '@angular/material';

import { MatPaginatorI18nService } from './_core/app-paginator/paginator-i18.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './_core/app-routing/app-routing.module';
import { AppMaterialModule } from './_core/app-material/app-material.module';
import {
  HeaderComponent,
  FooterComponent,
  SideNavComponent,
  SharedModule,
  SubHeaderComponent
} from './_shared';

import { LoginComponent } from './login/login.component';
import { AgentDashboardComponent } from './dashboard/agent-dashboard/agent-dashboard.component';
import { ChurchMembersComponent } from './church-members/church-members.component';
import { ChurchMembersDeleteComponent } from './church-members/church-members-delete/church-members-delete.component';
import { ChurchMembersDialogComponent } from './church-members/_dialogs/church-members-dialog/church-members-dialog.component';
import { ChurchMemberDetailsComponent } from './church-members/_dialogs/church-member-details/church-member-details.component';
import { FiltersDialogComponent } from './_shared-dialogs/filters-dialog/filters-dialog.component';
import { FamilyMemberComponent } from './church-members/_dialogs/family-member/family-member.component';
import { ChurchMemberPaymentComponent } from './church-members/_dialogs/church-member-payment/church-member-payment.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SideNavComponent,
    SubHeaderComponent,
    LoginComponent,
    AgentDashboardComponent,
    ChurchMembersComponent,
    ChurchMembersDeleteComponent,
    ChurchMembersDialogComponent,
    ChurchMemberDetailsComponent,
    FiltersDialogComponent,
    FamilyMemberComponent,
    ChurchMemberPaymentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    ChartsModule,
    Ng2TelInputModule,
    AppRoutingModule,
    AppMaterialModule,
    SharedModule,

    // ngx-translate and the loader module
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  entryComponents: [
    ChurchMembersDeleteComponent,
    ChurchMembersDialogComponent,
    ChurchMemberDetailsComponent,
    FiltersDialogComponent,
    FamilyMemberComponent,
    ChurchMemberPaymentComponent
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorI18nService,
    }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
