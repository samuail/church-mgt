import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar, PageEvent } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { ChurchMembersDeleteComponent } from './church-members-delete/church-members-delete.component';
import { ChurchMembersDialogComponent } from './_dialogs/church-members-dialog/church-members-dialog.component';
import { ChurchMemberDetailsComponent } from './_dialogs/church-member-details/church-member-details.component';
import { FiltersDialogComponent } from '../_shared-dialogs/filters-dialog/filters-dialog.component';

@Component({
  selector: 'app-church-members',
  templateUrl: './church-members.component.html',
  styleUrls: ['./church-members.component.css']
})
export class ChurchMembersComponent implements OnInit {

	resultsLength: number = 100;  
	pageSize = 10;
  	pageSizeOptions: number[] = [5, 10, 25, 100];
	pageEvent: PageEvent;

	dataSource = [];
  	activePageDataChunk = []

	pageName: string = "";

	search: string = '';
	inputSearch: boolean = false;
	
	checkedMembersHeader: boolean = false;
	indeterminateMembersHeader: boolean = false;
	checkedMembersOpt: boolean [];
	btnMembersOptHeader: boolean = false;
	btnMembersOpt: boolean [];
  
  	modelArr = [];

	constructor( private translate: TranslateService,
                public dialog: MatDialog, 
                public snackBar: MatSnackBar
                ) {
		for (let i = 1; i < 100; i++) {
			let dummyObject = { id: i, title: "This is the second task", status: false }
			this.dataSource.push(dummyObject);
		}
		this.activePageDataChunk = this.dataSource.slice(0, this.pageSize);
	}

	ngOnInit() {
		this.checkedMembersOpt = [];
		this.btnMembersOpt = [];
		this.translate.get('chMgt.parish').subscribe((res: string) => {
			this.pageName = res;
		});

		this.translate.get('chMgt.members').subscribe((res: string) => {
			this.pageName = this.pageName + " " + res;
		});

		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.translate.get('chMgt.parish').subscribe((res: string) => {
				this.pageName = res;
			});
	
			this.translate.get('chMgt.members').subscribe((res: string) => {
				this.pageName = this.pageName + " " + res;
			});
		});
	}

	className: string = 'blue-snackbar';

	deleteCount: BehaviorSubject<number> = new BehaviorSubject<number>(0);

	btnSelectAll(event){
		if ( event.target.checked ) {

		    this.deleteCount.next(this.dataSource.length);
		    if (this.snackBar._openedSnackBarRef == null) {	
			    this.snackBar.openFromComponent(ChurchMembersDeleteComponent, {
			      panelClass: [this.className],
			      data: this.deleteCount
			    });
			}
		} else {
			this.snackBar.dismiss();
		}
	}

	changeModel(event, list, memberId) {
		if(event.target.checked) {
			list.push(memberId);
			this.deleteCount.next(list.length);
			if (this.snackBar._openedSnackBarRef == null) {
				this.snackBar.openFromComponent(ChurchMembersDeleteComponent, {
			      panelClass: [this.className],
			      data: this.deleteCount
			    });
			}		   
		} else {
			let i = list.indexOf(memberId);
			list.splice(i, 1);
			if (list.length == 0) {
				this.snackBar.dismiss();
			} else {
				this.deleteCount.next(list.length);
			}
		}
	}

	isAllSelected() {
		const numSelected = this.modelArr.length;
		const numRows = this.dataSource.length;
		return numSelected === numRows;	
	}

	masterToggle() {
		this.isAllSelected() ?
		    this.modelArr.length = 0 :
		    this.dataSource.forEach(
		    	row => {
					if (!this.modelArr.includes(row.id)) this.modelArr.push(row.id); 
				});
	}

	clearSearch() {
		this.search = '';
	}

	addChurchMember() {
		const dialogRef = this.openChurchMembersDialog('Add');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	editChurchMember(member) {
		const dialogRef = this.openChurchMembersDialog('Edit');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	openChurchMembersDialog(title) {
		const dialogConfig = new MatDialogConfig();
      
		dialogConfig.data = {
			title: title  
		};

		dialogConfig.width = "800px";
	    dialogConfig.disableClose = true;
	    dialogConfig.autoFocus = true;


   		const dialogRef = this.dialog.open(ChurchMembersDialogComponent, dialogConfig);

    	return dialogRef;
	}

	churchMemberDetails(member) {
		const dialogRef = this.openChurchMemberDetailsDialog();
	  	dialogRef.afterClosed().subscribe(
			data => {
				console.log(data);
		});	
	}

	openChurchMemberDetailsDialog() {
		const dialogConfig = new MatDialogConfig();

	    dialogConfig.width = "70%";
	    dialogConfig.panelClass = 'member-detail-dialog-container';
	    dialogConfig.disableClose = true;
	    dialogConfig.autoFocus = true;

   		const dialogRef = this.dialog.open(ChurchMemberDetailsComponent, dialogConfig);

    	return dialogRef;
	}

	filterMemberList() {
		const dialogRef = this.openFiltersDialog('Filters');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	openFiltersDialog (title) {
		const dialogConfig = new MatDialogConfig();
	
		dialogConfig.data = {
			title: title  
		};

		dialogConfig.width = "260px";
		dialogConfig.height = "100vh";
		dialogConfig.position = { right: '0', top: '0'};
		dialogConfig.autoFocus = true;
		const dialogRef = this.dialog.open(FiltersDialogComponent, dialogConfig);

		return dialogRef;
	}

	getData(e) {
		let firstCut = e.pageIndex * e.pageSize;
    	let secondCut = firstCut + e.pageSize;
    	this.activePageDataChunk = this.dataSource.slice(firstCut, secondCut);
	}
}
