import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChurchMembersDeleteComponent } from './church-members-delete.component';

describe('ChurchMembersDeleteComponent', () => {
  let component: ChurchMembersDeleteComponent;
  let fixture: ComponentFixture<ChurchMembersDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChurchMembersDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChurchMembersDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
