import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-church-members-delete',
  templateUrl: './church-members-delete.component.html',
  styleUrls: ['./church-members-delete.component.css']
})
export class ChurchMembersDeleteComponent implements OnInit {

  status$: Observable<number>;

	constructor(@Inject(MAT_SNACK_BAR_DATA) public deleteCount: BehaviorSubject<number>) {
		this.status$ = this.deleteCount.asObservable();
	}

	ngOnInit() {
	}
}
