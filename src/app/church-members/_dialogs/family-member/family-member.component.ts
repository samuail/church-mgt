import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DateAdapter } from '@angular/material';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-family-member',
  templateUrl: './family-member.component.html',
  styleUrls: ['./family-member.component.css']
})
export class FamilyMemberComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
              private fb: FormBuilder,
              private translate: TranslateService,
              private dateAdapter: DateAdapter<any>,
              private dialogRef: MatDialogRef<FamilyMemberComponent>) { 
    this.title = data.title;
    this.dateAdapter.setLocale(this.translate.currentLang);
  }

  ngOnInit() {
    this.familyMemberForm = this.fb.group({
			fullName: ['', Validators.required],
			gender:  ['', Validators.required],
			dateOfBirth:  ['', Validators.required],
			christianName: ['', Validators.required],
      familyRelation: ['', Validators.required]
		});
  }

  title: string;
  loading: boolean = false;

  familyMemberForm: FormGroup;

  genders: any[] = [
		{value: '1', viewValue: 'Male'},
		{value: '2', viewValue: 'Female'}
  ];
  
  familyRelations: any[] = [
		{value: '1', viewValue: 'Mother'},
		{value: '2', viewValue: 'Father'}
  ];
  
  save() {
    this.loading = true;
  }

  close() {
	  this.dialogRef.close(false);
	}

}
