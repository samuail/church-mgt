import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-church-member-payment',
  templateUrl: './church-member-payment.component.html',
  styleUrls: ['./church-member-payment.component.css']
})
export class ChurchMemberPaymentComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
              private fb: FormBuilder,
              private translate: TranslateService,
              private dialogRef: MatDialogRef<ChurchMemberPaymentComponent>) { 
    this.title = data.title;
    this.translate.get('chMgt.months.september').subscribe((res: string) => {
			this.months[0].viewValue = res;
		});
		this.translate.get('chMgt.months.october').subscribe((res: string) => {
			this.months[1].viewValue = res;		
    });
    this.translate.get('chMgt.months.november').subscribe((res: string) => {
			this.months[2].viewValue = res;
		});
		this.translate.get('chMgt.months.december').subscribe((res: string) => {
			this.months[3].viewValue = res;		
    });
    this.translate.get('chMgt.months.january').subscribe((res: string) => {
			this.months[4].viewValue = res;
		});
		this.translate.get('chMgt.months.feburary').subscribe((res: string) => {
			this.months[5].viewValue = res;		
    });
    this.translate.get('chMgt.months.march').subscribe((res: string) => {
			this.months[6].viewValue = res;
		});
		this.translate.get('chMgt.months.april').subscribe((res: string) => {
			this.months[7].viewValue = res;		
    });
    this.translate.get('chMgt.months.may').subscribe((res: string) => {
			this.months[8].viewValue = res;
		});
		this.translate.get('chMgt.months.june').subscribe((res: string) => {
			this.months[9].viewValue = res;		
    });
    this.translate.get('chMgt.months.july').subscribe((res: string) => {
			this.months[10].viewValue = res;
		});
		this.translate.get('chMgt.months.august').subscribe((res: string) => {
			this.months[11].viewValue = res;		
    });
  }

  ngOnInit() {
    this.churchMemberPaymentForm = this.fb.group({
			month: ['', Validators.required],
			amount:  ['', Validators.required]
		});
  }

  title: string;
  loading: boolean = false;

  churchMemberPaymentForm: FormGroup;

  months: any[] = [
		{value: '1', viewValue: 'September'},
    {value: '2', viewValue: 'October'},
    {value: '3', viewValue: 'November'},
    {value: '4', viewValue: 'December'},
    {value: '5', viewValue: 'January'},
    {value: '6', viewValue: 'Feburary'},
    {value: '7', viewValue: 'March'},
    {value: '8', viewValue: 'April'},
    {value: '9', viewValue: 'May'},
    {value: '10', viewValue: 'June'},
    {value: '11', viewValue: 'July'},
    {value: '12', viewValue: 'August'}
  ];

  save() {
    this.loading = true;
  }

  close() {
	  this.dialogRef.close(false);
	}

}
