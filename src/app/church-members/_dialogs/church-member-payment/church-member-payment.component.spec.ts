import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChurchMemberPaymentComponent } from './church-member-payment.component';

describe('ChurchMemberPaymentComponent', () => {
  let component: ChurchMemberPaymentComponent;
  let fixture: ComponentFixture<ChurchMemberPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChurchMemberPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChurchMemberPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
