import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChurchMemberDetailsComponent } from './church-member-details.component';

describe('ChurchMemberDetailsComponent', () => {
  let component: ChurchMemberDetailsComponent;
  let fixture: ComponentFixture<ChurchMemberDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChurchMemberDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChurchMemberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
