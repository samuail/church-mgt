import { Component, OnInit, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MatTabChangeEvent, MatDialogConfig } from '@angular/material';

import { FamilyMemberComponent } from './../family-member/family-member.component';
import { ChurchMemberPaymentComponent } from './../church-member-payment/church-member-payment.component';

@Component({
  selector: 'app-church-member-details',
  templateUrl: './church-member-details.component.html',
  styleUrls: ['./church-member-details.component.css']
})
export class ChurchMemberDetailsComponent implements OnInit {

	addFamilyMemberBtn: boolean = false;
	addPaymentBtn: boolean = false;
	
	today: number = Date.now();  

	constructor(private dialogRef: MatDialogRef<ChurchMemberDetailsComponent>, public dialog: MatDialog,) {
  	}

	ngOnInit() {
	}

	onContactSave = new EventEmitter();

	save() {
	    this.onContactSave.emit(this);
	}

	close() {
	  this.dialogRef.close(false);
	}

	contactDetailTabsChanged = (tabChangeEvent: MatTabChangeEvent): void => {
		if (tabChangeEvent.index == 0)	{
			this.addFamilyMemberBtn = false;
			this.addPaymentBtn = false;
		} else if (tabChangeEvent.index == 1)	{
			this.addFamilyMemberBtn = true;
			this.addPaymentBtn = false;
		} else if (tabChangeEvent.index == 2)	{		
			this.addFamilyMemberBtn = false;
			this.addPaymentBtn = true;
		} 
	}

	addFamilyMember() {
		const dialogRef = this.openFamilyMembersDialog('Add');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	familyMemberDetails() {
		const dialogRef = this.openFamilyMembersDialog('Preview');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	editFamilyMember() {
		const dialogRef = this.openFamilyMembersDialog('Edit');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	deleteFamilyMember() {

	}

	openFamilyMembersDialog(title) {
		const dialogConfig = new MatDialogConfig();
      
		dialogConfig.data = {
			title: title  
		};

		dialogConfig.width = "50vh";
	    dialogConfig.disableClose = true;
	    dialogConfig.autoFocus = true;


   		const dialogRef = this.dialog.open(FamilyMemberComponent, dialogConfig);

    	return dialogRef;
	}


	addPayment() {
		const dialogRef = this.openChurchMemberPaymentDialog('Add');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	editPayment() {
		const dialogRef = this.openChurchMemberPaymentDialog('Edit');
	    dialogRef.afterClosed().subscribe(
	        data => {
	          console.log(data);
		});
	}

	openChurchMemberPaymentDialog(title) {
		const dialogConfig = new MatDialogConfig();
      
		dialogConfig.data = {
			title: title  
		};

		dialogConfig.width = "40vh";
	    dialogConfig.disableClose = true;
	    dialogConfig.autoFocus = true;


   		const dialogRef = this.dialog.open(ChurchMemberPaymentComponent, dialogConfig);

    	return dialogRef;		
	}

}
