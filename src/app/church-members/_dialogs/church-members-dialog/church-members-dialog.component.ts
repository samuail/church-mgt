import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { DateAdapter } from '@angular/material';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-church-members-dialog',
  templateUrl: './church-members-dialog.component.html',
  styleUrls: ['./church-members-dialog.component.css'],
  animations: [
    trigger('selectedImage', [
      state('hideImage', style({
        opacity: 0,
      })),
      state('showImage', style({
        opacity: 1
      })),
      transition('showImage=>hideImage',[
        animate(0)
      ]),
      transition('hideImage=>showImage',[
        animate(650)
      ])
    ])
  ]
})
export class ChurchMembersDialogComponent implements OnInit {

	title: string;
	loading: boolean = false;
  	imageState = "showImage";

  	churchMemberForm: FormGroup;

  	genders: any[] = [
		{value: '1', viewValue: 'Male'},
		{value: '2', viewValue: 'Female'}
	];
	
	countries: any[] = [
		{ value: '1', viewValue: 'Ethiopia' },
		{ value: '2', viewValue: 'United Arab Emirates' },
		{ value: '3', viewValue: 'United States of America' }
	];

	cities: any[] = [
		{ value: '1', viewValue: 'Addis Ababa' },
		{ value: '2', viewValue: 'Bahir Dar' },
		{ value: '3', viewValue: 'Jimma' }
	];

	subCities: any[] = [
		{ value: '1', viewValue: 'Arada' },
		{ value: '2', viewValue: 'Bole' },
		{ value: '3', viewValue: 'Yeka' }
	];

	constructor(@Inject(MAT_DIALOG_DATA) public data: any,
				private fb: FormBuilder,
				private translate: TranslateService,
				private dateAdapter: DateAdapter<any>,
				private dialogRef: MatDialogRef<ChurchMembersDialogComponent>) { 
		this.title = data.title;
		this.translate.get('chMgt.church-member.genderMale').subscribe((res: string) => {
			this.genders[0].viewValue = res;
		});
		this.translate.get('chMgt.church-member.genderFemale').subscribe((res: string) => {
			this.genders[1].viewValue = res;		
		});

		this.dateAdapter.setLocale(this.translate.currentLang);
	}

	ngOnInit() {
		this.churchMemberForm = this.fb.group({
			fullName: ['', Validators.required],
			gender:  ['', Validators.required],
			dateOfBirth:  ['', Validators.required],
			placeOfBirth: ['', Validators.required],
			fathersName: ['', Validators.required],
			mothersName: ['', Validators.required],
			christianName: ['', Validators.required],
			baptizedChurch: ['', Validators.required],
			country: ['', Validators.required],
			city: ['', Validators.required],
			subCity: ['', Validators.required],
			woreda: ['', Validators.required],
			kebele: ['', Validators.required],
			hNo: ['', Validators.required],
			phoneNumber: ['', Validators.required],
			priestName: ['', Validators.required],
			priestChurch: ['', Validators.required]
		});
	}

	hasError(event: any): void {
	    if (!event && this.churchMemberForm.value.phoneNumber !== '') {
	    	this.churchMemberForm.get('phoneNumber').setErrors(['invalid_cell_phone', true]);
	    }
	}

	onContactSave = new EventEmitter();

	save() {
		this.onContactSave.emit(this);
		this.loading = true;
	}

	close() {
	  this.dialogRef.close(false);
	}

	url: string;

	importFile(event) {
		this.imageState = "hideImage";
		if (event.target.files && event.target.files[0]) {
			var reader = new FileReader();
					
		
			reader.onload = (e: any) => { 
				this.url = e.target.result;
				this.imageState = "showImage";
			}

			reader.readAsDataURL(event.target.files[0]); 
		}
	}
}