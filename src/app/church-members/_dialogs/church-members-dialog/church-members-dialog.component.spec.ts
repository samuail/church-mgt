import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChurchMembersDialogComponent } from './church-members-dialog.component';

describe('ChurchMembersDialogComponent', () => {
  let component: ChurchMembersDialogComponent;
  let fixture: ComponentFixture<ChurchMembersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChurchMembersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChurchMembersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
