import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChurchMembersComponent } from './church-members.component';

describe('ChurchMembersComponent', () => {
  let component: ChurchMembersComponent;
  let fixture: ComponentFixture<ChurchMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChurchMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChurchMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
