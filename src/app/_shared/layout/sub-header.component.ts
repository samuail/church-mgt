import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-layout-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {
  
  @Input() pageName: string;

  checkedShowHide: boolean = false;

  url: string;
  my_clients: boolean;

  constructor (private route: ActivatedRoute) {
    this.url = this.route.snapshot.url[0].path;
    if (this.url === "my_clients") {
      this.my_clients = true;
    }
  }

  ngOnInit() {
  }

  showHide(event) {
    this.checkedShowHide = event.checked;
  }
}
