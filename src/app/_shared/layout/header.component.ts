import { Component, OnInit, EventEmitter, Output, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService } from '../../_services/authentication.service';
import { User } from '../../_models/user';


@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  appName = 'Medhanealem Church Management';

  @Output() toggleSidenav = new EventEmitter<void>();

  homeLink: string = "\agent";

  btnContacts: boolean = false;
  btnOpprtunities: boolean = false;
  btnDeals: boolean = false;
  btnPlanner: boolean = false;
  btnCalendar: boolean = false;
  btnNotes: boolean = false;
  btnFiles: boolean = false;
  btnProfessionals: boolean = false;
  btnMarketing: boolean = false;
  btnTodayTask: boolean = false;
  taskRadio: boolean = true;

  today: number = Date.now();
  todayTasks: any[] = [{title: "This is the first Task!", status: true }];

  url: string;

  currentUser: User; 

  constructor ( private route: ActivatedRoute,
                private router: Router, 
                private authenticationService: AuthenticationService,
                private translate: TranslateService,
                private renderer: Renderer2) { 
    this.todayTasks.push({title: "This is the second task", status: false });
    if (this.route.snapshot.url.length > 0){
      this.url = this.route.snapshot.url[0].path;
      if (this.url === "my_clients") {
        this.btnContacts = true;
      }
    }
  }

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  clickEvent(item) {
    if (item == 'contacts') {
      this.btnContacts = true;
      this.btnOpprtunities = false;
      this.btnDeals = false;
      this.btnPlanner = false;
      this.btnCalendar = false;
      this.btnNotes = false;
      this.btnFiles = false;
      this.btnProfessionals = false;
      this.btnMarketing = false;
    } else if (item == 'opprtunities') {
      this.btnContacts = false;
      this.btnOpprtunities = true;
      this.btnDeals = false;
      this.btnPlanner = false;
      this.btnCalendar = false;
      this.btnNotes = false;
      this.btnFiles = false;
      this.btnProfessionals = false;
      this.btnMarketing = false;
    } else if (item == 'deals') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = true;
        this.btnPlanner = false;
        this.btnCalendar = false;
        this.btnNotes = false;
        this.btnFiles = false;
        this.btnProfessionals = false;
        this.btnMarketing = false;
    } else if (item == 'planner') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = true;
        this.btnCalendar = false;
        this.btnNotes = false;
        this.btnFiles = false;
        this.btnProfessionals = false;
        this.btnMarketing = false;
    } else if (item == 'calendar') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = false;
        this.btnCalendar = true;
        this.btnNotes = false;
        this.btnFiles = false;
        this.btnProfessionals = false;
        this.btnMarketing = false;
    } else if (item == 'notes') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = false;
        this.btnCalendar = false;
        this.btnNotes = true;
        this.btnFiles = false;
        this.btnProfessionals = false;
        this.btnMarketing = false;
    } else if (item == 'files') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = false;
        this.btnCalendar = false;
        this.btnNotes = false;
        this.btnFiles = true;
        this.btnProfessionals = false;
        this.btnMarketing = false;
    } else if (item == 'professionals') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = false;
        this.btnCalendar = false;
        this.btnNotes = false;
        this.btnFiles = false;
        this.btnProfessionals = true;
        this.btnMarketing = false;
    } else if (item == 'marketing') {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = false;
        this.btnCalendar = false;
        this.btnNotes = false;
        this.btnFiles = false;
        this.btnProfessionals = false;
        this.btnMarketing = true;
    } else {
        this.btnContacts = false;
        this.btnOpprtunities = false;
        this.btnDeals = false;
        this.btnPlanner = false;
        this.btnCalendar = false;
        this.btnNotes = false;
        this.btnFiles = false;
        this.btnProfessionals = false;
        this.btnMarketing = false;
    }
  }

  showHide(status) {
    this.btnTodayTask = status;
  }

  onLogout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  languages = ["en", "አማ"];
  selectedLanguage: string = "en";  

  onLanguageSelect({ value: language }) {
    this.selectedLanguage = language;
    if (language == "አማ")
      language = "am";
    this.translate.use(language);
    this.renderer.setAttribute(document.querySelector('html'), 'lang', language);
  }

  useLanguage(language: string) {
    this.translate.use(language);
  }

}
