import { Component, OnInit, Input } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-layout-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})

export class SideNavComponent implements OnInit {
  
  @Input() sideNav: MatSidenav;

  userMenus: any [] = [];

  constructor () {
  }	

  ngOnInit() {
    this.userMenus.push({ id: 1, iconName: "settings_applications", displayName: "Business Settings", 
                          children: [ { iconName: "group", displayName: "Chruch Members", route: "members" }, 
                                      { iconName: "home", displayName: "Properties", route: "members" }] });
  }

}
