import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-filters-dialog',
  templateUrl: './filters-dialog.component.html',
  styleUrls: ['./filters-dialog.component.css']
})
export class FiltersDialogComponent implements OnInit {

  title: string;
	collapsedGender: boolean = true;
	collapsedAgeRanges: boolean = true;
	collapsedName: boolean = true;
	alphabets: any [] = [];

	genderGroupCount: number = 0;
	contactGenderChkBox: boolean = false;

	ageRangesCount: number = 0;

  maleChecked: boolean = false;
  femaleChecked: boolean = false;

	ageRange18_25Checked: boolean = false;
	ageRange26_40Checked: boolean = false;
	ageRange41_60Checked: boolean = false;
	ageRangeGt_60Checked: boolean = false;

  contactAgeRangesChkBox: boolean = false;

	contactName: string = "";
	contactNameCount: number = 0;

  	constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  				private fb: FormBuilder,
				private dialogRef: MatDialogRef<FiltersDialogComponent>) { 
		this.title = data.title;

		for (let i = 65; i <= 90;i++) {
		    this.alphabets.push(String.fromCharCode(i));
		}
	}

	ngOnInit() {
  }

  toggleGender() {
    this.collapsedGender = ! this.collapsedGender;
  }

  toggleAgeRanges() {
    this.collapsedAgeRanges = ! this.collapsedAgeRanges;
  }

  toggleName() {
    this.collapsedName = ! this.collapsedName;
  }

  maleGroupChk() {	  
    if(this.maleChecked){
      this.contactGenderChkBox = true;
      this.genderGroupCount =  this.genderGroupCount + 1;	
    } else {      
      this.genderGroupCount =  this.genderGroupCount - 1;
      if(!this.femaleChecked){
        this.contactGenderChkBox = false;
      }
    }
  }
  
  femaleGroupChk() {	  
    if(this.femaleChecked){
      this.contactGenderChkBox = true;
      this.genderGroupCount =  this.genderGroupCount + 1;	
    } else {    
      this.genderGroupCount =  this.genderGroupCount - 1;
      if(!this.maleChecked){
        this.contactGenderChkBox = false;
      }
    }
	}

	ageRange18_25Chk() {
		if(this.ageRange18_25Checked){
	      	this.contactAgeRangesChkBox = true;
	      	this.ageRangesCount =  this.ageRangesCount + 1;	
	    } else {
	      	this.ageRangesCount =  this.ageRangesCount - 1;
	      	if (!this.ageRange26_40Checked && !this.ageRange41_60Checked && !this.ageRangeGt_60Checked) {
	      		this.contactAgeRangesChkBox = false;
	      	}
	    }
	}

	ageRange26_40Chk() {
		if(this.ageRange26_40Checked){
	      	this.contactAgeRangesChkBox = true;
	      	this.ageRangesCount =  this.ageRangesCount + 1;	
	    } else {
	      	this.ageRangesCount =  this.ageRangesCount - 1;
	      	if (!this.ageRange18_25Checked && !this.ageRange41_60Checked && !this.ageRangeGt_60Checked) {
	      		this.contactAgeRangesChkBox = false;
	      	}
	    }
	}

	ageRange41_60Chk() {
		if(this.ageRange41_60Checked){
	      	this.contactAgeRangesChkBox = true;
	      	this.ageRangesCount =  this.ageRangesCount + 1;	
	    } else {
	      	this.ageRangesCount =  this.ageRangesCount - 1;
	      	if (!this.ageRange18_25Checked && !this.ageRange26_40Checked && !this.ageRangeGt_60Checked) {
	      		this.contactAgeRangesChkBox = false;
	      	}
	    }
	}

	ageRangeGt_60Chk() {
		if(this.ageRangeGt_60Checked){
	      	this.contactAgeRangesChkBox = true;
	      	this.ageRangesCount =  this.ageRangesCount + 1;	
	    } else {
	      	this.ageRangesCount =  this.ageRangesCount - 1;
	      	if (!this.ageRange18_25Checked && !this.ageRange26_40Checked && !this.ageRange41_60Checked) {
	      		this.contactAgeRangesChkBox = false;
	      	}
	    }
	}

	contactNameInitials() {
		if (this.contactNameCount <= 0) {
			this.contactNameCount = this.contactNameCount + 1;
		}
	}

  	onapplyFilter = new EventEmitter();


  	save() {
	    this.onapplyFilter.emit(this);
	}

	close() {
	  this.dialogRef.close(false);
	}

}
