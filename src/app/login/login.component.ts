import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services/authentication.service';

import { User } from '../_models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	loginForm: FormGroup;

	loading = false;

	currentUser: User;

	constructor(private fb: FormBuilder, 
				private router: Router,
    			private authenticationService: AuthenticationService) { }

	ngOnInit() {
		this.loginForm = this.fb.group({     
	      	userName: ['', Validators.required],
	      	password: ['', Validators.required]
	    });
	}

	onSubmit() {
	    if (this.loginForm.invalid) {
        	return;
    	}
    	this.loading = true;
    	this.authenticationService.login(this.loginForm.value.userName, this.loginForm.value.password);
    	this.authenticationService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
    	this.router.navigate(['/agent']);
    }
}
