import { MatPaginatorIntl } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

const ITEMS_PER_PAGE = 'chMgt.paginator.Items_per_page';
const NEXT_PAGE = 'chMgt.paginator.Next_page';
const PREV_PAGE = 'chMgt.paginator.Previous_page';
const FIRST_PAGE = 'chMgt.paginator.First_page';
const LAST_PAGE = 'chMgt.paginator.Last_page';

@Injectable()
export class MatPaginatorI18nService extends MatPaginatorIntl {
    public constructor(private translate: TranslateService) {
        super();

        this.translate.onLangChange.subscribe((e: Event) => {
            this.getAndInitTranslations();
        });

        this.getAndInitTranslations();
    }

    public getRangeLabel = (page: number, pageSize: number, length: number): string => {
        const of = this.translate ? this.translate.instant('chMgt.paginator.of') : 'of';
        if (length === 0 || pageSize === 0) {
          return '0 ' + of + ' ' + length;
        }
        length = Math.max(length, 0);
        const startIndex = ((page * pageSize) > length) ?
          (Math.ceil(length / pageSize) - 1) * pageSize:
          page * pageSize;
    
        const endIndex = Math.min(startIndex + pageSize, length);
        return startIndex + 1 + ' - ' + endIndex + ' ' + of + ' ' + length;
      };

    public getAndInitTranslations(): void {
        this.translate.get([
            ITEMS_PER_PAGE,
            NEXT_PAGE,
            PREV_PAGE,
            FIRST_PAGE,
            LAST_PAGE,
        ]).subscribe((translation: any) => {
            this.itemsPerPageLabel = translation[ITEMS_PER_PAGE];
            this.nextPageLabel = translation[NEXT_PAGE];
            this.previousPageLabel = translation[PREV_PAGE];
            this.firstPageLabel = translation[FIRST_PAGE];
            this.lastPageLabel = translation[LAST_PAGE];

            this.changes.next();
        });
    }
}
