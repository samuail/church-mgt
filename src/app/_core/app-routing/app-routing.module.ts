import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// import { AppComponent } from '../../app.component';
import { LoginComponent } from '../../login/login.component';
import { AgentDashboardComponent } from '../../dashboard/agent-dashboard/agent-dashboard.component';
import { ChurchMembersComponent } from '../../church-members/church-members.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'agent', component: AgentDashboardComponent },
  { path: 'members', component: ChurchMembersComponent }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
