export interface ChruchMember {
    id: number;
    fullName: string;
    gender: string;
    christianName: string;
    baptizedChurch: string;
    dateOfBirth: string;
    placeOfBirth: string;
    nationality: string;
    fathersName: string;
    mothersName: string;
    country: string;
    city: string;
    subCity: string;
    woreda: string;
    kebele: string;
    hNo: string;
    telephone: string;
    priestName: string;
    priestChurch: string;
    active: boolean;
}