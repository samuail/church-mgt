import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
// import { BehaviorSubject, Observable } from 'rxjs';
// import { DOCUMENT } from '@angular/common';
// import { map } from 'rxjs/operators';

// import { ShowmenuService } from './_core/showmenu.service';
// import defaultLanguage from './../assets/i18n/en.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'church-mgt';
  isprofile = false;

  constructor(  private _router:Router, 
                // public showmenu: ShowmenuService,
                private translate: TranslateService) {
      // translate.setTranslation('en', defaultLanguage);
      translate.setDefaultLang('en');
    }

  ngOnInit(){    
    this._router.navigate(['members']);
  }

  @ViewChild('sidenav', {static: false}) public sidenav;

	@ViewChild('mainContent', {static: false})
  mainContent: ElementRef;

  @ViewChild('toolbarContent', {static: false})
  toolbarContent: ElementRef;
  	
	
  // appName = 'Medhanealem Church Management';
  // appName = 'መድኃኒዓለም ቤተክርስቲያን አስተዳደር';

	homeLink: string;
  marginTop: any;
  
}
