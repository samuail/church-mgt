import { Component, OnInit, Input } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import { SingleDataSet, Label } from 'ng2-charts';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-agent-dashboard',
  templateUrl: './agent-dashboard.component.html',
  styleUrls: ['./agent-dashboard.component.css']
})
export class AgentDashboardComponent implements OnInit {

  	@Input() value: number = 82
	@Input() value2: number = 50
	@Input() value3: number = 50

	public circumference: number = 2 * Math.PI * 20
	public strokeDashoffset: number = 20
	public strokeDashoffset2: number = 55
	public strokeDashoffset3: number = 40
	public strokeDashoffset4: number = 30

	public usersChartLabels: string [] = [];
	public usersChartData:number[] = [30, 10, 5];
	public usersChartType:string = 'doughnut';

	public usersChartOptions: ChartOptions = {
		responsive: true,		
		tooltips: {
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			// position: 'left',
			display: true,
			labels: {
				fontFamily: "titillium web,sans-serif"
			}
		}
	};
  	public barChartData: ChartDataSets [] = [
		{ data: [65, 59, 80, 81, 56, 55, 40], label: '' },
		{ data: [28, 48, 40, 19, 86, 27, 90], label: '' },
		{ data: [30, 65, 50, 60, 67, 25, 30], label: ''}
	];

	parish: string = "";
	member: string = "";
  constructor(private translate: TranslateService, private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
	this.translate.get('chMgt.baptism').subscribe((res: string) => {
		this.usersChartLabels.push(res);
		this.barChartData[0]["label"] = res;		
	});
	this.translate.get('chMgt.wedding').subscribe((res: string) => {
		this.usersChartLabels.push(res);
		this.barChartData[1]["label"] = res;
	});
	this.translate.get('chMgt.parish').subscribe((res: string) => {
		this.parish = res;
		this.barChartData[2]["label"] = res;
	});
	this.translate.get('chMgt.member').subscribe((res: string) => {
		this.member = res;
		this.barChartData[2]["label"] = this.barChartData[2]["label"] + " " + res;
	});

	this.usersChartLabels.push(this.parish + " " + this.member);

	this.breakpointObserver.observe(['(min-width: 1279px)']).subscribe(result => {
		if (result.matches) {
			this.usersChartOptions["legend"]["position"] = 'left';
		} else {
			this.usersChartOptions["legend"]["position"] = 'top';
		}
	});

	this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
		this.translate.get('chMgt.baptism').subscribe((res: string) => {
			this.usersChartLabels[0]= res;
			this.barChartData[0]["label"] = res;		
		});
		this.translate.get('chMgt.wedding').subscribe((res: string) => {
			this.usersChartLabels[1]= res;
			this.barChartData[1]["label"] = res;
		});
		this.translate.get('chMgt.parish').subscribe((res: string) => {
			this.parish = res;
			this.barChartData[2]["label"] = res;
		});
		this.translate.get('chMgt.member').subscribe((res: string) => {
			this.member = res;
			this.barChartData[2]["label"] = this.barChartData[2]["label"] + " " + res;
		});

		this.usersChartLabels[2] = this.parish + " " + this.member;

	});
  }

  // Users

	// events
	public usersChartClicked(e:any):void {
	// console.log(e);
	}

	public usersChartHovered(e:any):void {
	// console.log(e);
	}

	// PolarArea
  public polarAreaChartLabels: Label[] = ['Agent Sales', 'Self Sales', 'Corporate Sales'];
  public polarAreaChartData: SingleDataSet = [100, 40, 120];
  public polarAreaLegend = true;

  public polarAreaChartType: ChartType = 'polarArea';

    // events
	public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
		console.log(event, active);
	}

	public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
		console.log(event, active);
	}	

	//Bar chart

	public barChartOptions: ChartOptions = {
		responsive: true,
		tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
		scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
	};
	public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
	public barChartType: ChartType = 'bar';
	public barChartLegend = true;
	public barChartPlugins = [];	
}
